package com.example.demo.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public record ProductRequestDTO(
        @NotBlank @Size(max = 100) String name,
        @NotBlank @Size(max = 255) String description,
        @NotNull @Min(0) double price) {
}