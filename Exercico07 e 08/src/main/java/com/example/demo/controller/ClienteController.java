package com.example.demo.controller;

import com.example.demo.dto.ClienteCreateDTO;
import com.example.demo.dto.ClienteDTO;
import com.example.demo.service.ClienteService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
    @Autowired
    private ClienteService clienteService;

    @GetMapping("/{nome}")
    public ClienteDTO getCliente(@PathVariable String nome){
        return clienteService.getClienteByNome(nome);
    }

    @PostMapping
    public ClienteDTO adicionarCliente(@Valid @RequestBody ClienteCreateDTO dto) {
        return clienteService.adicionarCliente(new ClienteDTO(dto.nome(), dto.saldo()), dto.senha());
    }
}
