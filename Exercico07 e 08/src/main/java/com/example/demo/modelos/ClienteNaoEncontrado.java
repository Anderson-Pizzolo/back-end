package com.example.demo.modelos;

public class ClienteNaoEncontrado extends RuntimeException{
    public ClienteNaoEncontrado(String mensagem){
        super(mensagem);
    }
}
