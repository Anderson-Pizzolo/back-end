package com.example.demo.modelos;

public class SaldoInsuficiente extends RuntimeException{
    public SaldoInsuficiente(String mensagem){
        super(mensagem);
    }
}
