package com.example.demo.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public record ClienteDTO(@NotEmpty String nome, @NotNull Double saldo) {
}
