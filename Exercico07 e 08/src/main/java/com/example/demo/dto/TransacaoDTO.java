package com.example.demo.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

public record TransacaoDTO(@NotEmpty (message = "O campo 'recebedor' não pode estar vazio") String recebedor,
                           @NotEmpty (message = "O campo 'pagador' não pode estar vazio") String pagador,
                           @NotNull(message = "O campo 'quantidade' não pode ser nulo")
                           @Positive (message = "A 'quantidade' deve ser maior que 0") Double quantidade) {
}
