package com.example.demo.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public record ClienteCreateDTO(@NotEmpty (message = "O campo 'nome' não pode estar vazio") String nome,
                               @NotNull (message = "O campo 'saldo' não pode ser nulo") Double saldo,
                               @NotEmpty (message = "O campo 'senha' não pode estar vazio") String senha) {
}
