package com.example.demo.mappers;

import com.example.demo.dto.ClienteDTO;
import com.example.demo.modelos.Cliente;

public class ClienteMapper {
    public static ClienteDTO toDTO(Cliente cliente) {
        return new ClienteDTO(cliente.getNome(), cliente.getSaldo());
    }
}
