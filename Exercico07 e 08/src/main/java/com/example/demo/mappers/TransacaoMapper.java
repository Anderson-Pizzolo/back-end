package com.example.demo.mappers;

import com.example.demo.dto.TransacaoDTO;
import com.example.demo.modelos.Transacao;

public class TransacaoMapper {
    public static TransacaoDTO toDTO(Transacao transacao) {
        return new TransacaoDTO(transacao.getRecebedor(), transacao.getPagador(), transacao.getQuantidade());
    }

    public static Transacao toEntity(TransacaoDTO dto) {
        return new Transacao(dto.recebedor(), dto.pagador(), dto.quantidade());
    }
}
