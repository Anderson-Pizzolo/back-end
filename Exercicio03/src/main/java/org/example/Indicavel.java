package org.example;


public interface Indicavel {
    String getNome();
    Boolean isElegivel();
    Short getNumeroDeIndicacoes();
}
