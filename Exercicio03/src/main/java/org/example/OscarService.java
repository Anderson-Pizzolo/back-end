package org.example;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class OscarService {

    private List<Indicacao> indicados = new ArrayList<>();

    public void adicionarIndicacao(Indicacao indicacao) {
        indicados.add(indicacao);
    }

    public void mostrarListaIndicados() {
        for (Indicacao indicacao : indicados) {
            System.out.println("Indicado: " + indicacao.getIndicavel().getNome() + ", Categoria: " + indicacao.getCategoria());
        }
    }
}
