package org.example;

public class Main{
 public static void main(String[] args) {
  OscarService oscarService = new OscarService();


  Filme filme = new Filme("Titanic  ", "Drama", true, (short) 11);

  oscarService.adicionarIndicacao(new Indicacao(filme, "Melhor Filme"));

  Ator ator = new Ator("Leonardo DiCaprio  ", "Estados Unidos", true, (short) 6);

  oscarService.adicionarIndicacao(new Indicacao(ator, "Melhor Ator"));

  oscarService.mostrarListaIndicados();
 }
}


