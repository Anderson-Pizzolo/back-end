package org.example;

public class Ator implements Indicavel {

    private String nome;
    private String nacionalidade;
    private Boolean elegivel;
    private Short numeroDeIndicacoes;

    public Ator(String nome, String nacionalidade, Boolean elegivel, Short numeroDeIndicacoes) {
        this.nome = nome;
        this.nacionalidade = nacionalidade;
        this.elegivel = elegivel;
        this.numeroDeIndicacoes = numeroDeIndicacoes;
    }

    public String getNome() {
        return nome;
    }

    @Override
    public Boolean isElegivel() {
        return null;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    public Boolean getElegivel() {
        return elegivel;
    }

    public void setElegivel(Boolean elegivel) {
        this.elegivel = elegivel;
    }

    public Short getNumeroDeIndicacoes() {
        return numeroDeIndicacoes;
    }

    public void setNumeroDeIndicacoes(Short numeroDeIndicacoes) {
        this.numeroDeIndicacoes = numeroDeIndicacoes;
    }
}
