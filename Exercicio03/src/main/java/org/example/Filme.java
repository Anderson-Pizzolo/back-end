package org.example;


public class Filme implements Indicavel {
    private String nome;
    private String genero;
    private Boolean elegivel;
    private Short numeroDeIndicacoes;


    public Filme(String nome, String genero, Boolean elegivel, Short numeroDeIndicacoes) {
        this.nome = nome;
        this.genero = genero;
        this.elegivel = elegivel;
        this.numeroDeIndicacoes = numeroDeIndicacoes;
    }

    @Override
    public String getNome() {
        return nome;
    }

    public String getGenero() {
        return genero;
    }

    @Override
    public Boolean isElegivel() {
        return elegivel;
    }

    @Override
    public Short getNumeroDeIndicacoes() {
        return numeroDeIndicacoes;
    }
}

