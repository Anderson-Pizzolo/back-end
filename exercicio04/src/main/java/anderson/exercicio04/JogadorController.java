package anderson.exercicio04;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JogadorController {
    @Autowired
    private final GerarJogador geradorJogador;

    public JogadorController(GerarJogador geradorJogador) {
        this.geradorJogador = geradorJogador;
        this.geradorJogador.atualizaListas();
    }

    @GetMapping("/")
    public Jogador geraJogador() {
        return geradorJogador.geraJogador();
    }

}
